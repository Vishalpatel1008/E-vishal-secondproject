import React from 'react'
import {useQuery} from '@apollo/client'
import CATEGORY_QUERY from '../queries/category/category'
function Product() {
const {loading, error, data} =  useQuery(CATEGORY_QUERY)
if(loading) return <p>Loading...</p>
if(error) return <p>Error:</p>
  // const { loading, error, data } = useFetch(
  //   // "http://localhost:1337/api/categories"
  //   "http://localhost:1337/graphql",{
  //     // method:"GET",
  //   }
  // );
//   console.log(data);
  // if (loading) return <p>Loading...</p>;
  // if (error) return <p>Error :(</p>;

  return (
    <div class="container">
      {data.categories.data.map(item => (
            <div className='card m-2 p-2'>
               <h6 class="text-start ml-3 text-primary">
                   {item.attributes.name}
               </h6>
            </div>

           ))}
    </div>
  );
}
export default Product
