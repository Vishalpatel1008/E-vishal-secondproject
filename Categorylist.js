import React from 'react'
import { Link, useHistory } from "react-router-dom";
import {useQuery,useMutation} from '@apollo/client';
import CATEGORY_QUERY from '../queries/category/category';
function Categorylist() {
    const {loading, error, data} =  useQuery(CATEGORY_QUERY);
    if(loading) return <p>Loading...</p>
    if(error) return <p>Error:</p>
    return (
        <div class="container-fluid">
        <div className='site-header text-center mt-3 mb-3'>
        <h1>Category List</h1>
         {data.categories.map(item =>(
             <div class="card m-2 p-2 row" key={item.id}>
                 <h3 class="text-start ml-3 fw-bold text-primary">{item.name}</h3>
                 {
                     item.restaurants.map(data=>(
                         <div class="text-start">
                         <h5>{data.name}</h5>
                         <div>{data.description}</div>
                         </div>
                     )
                     )
                 }
             </div>
         ))}
    </div>
    </div>
    )
}
export default Categorylist